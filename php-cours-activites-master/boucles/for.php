<?php

/*****************************************/
/********** Les boucles : For ************/
/*****************************************/
echo '1.Nombre de mouton : <br>';
for ($i = 0; $i <= 10; ++$i) {
    echo $i.' mouton(s)<br>';
}
echo '<br><br>';
//----------------------------------------
//for : particulièrement utile pour pacourir un tableau
$couleurs = ['rouge', 'bleu', 'vert', 'orange', 'marron', 'noir', 'blanc'];
//count est une fonction proposée par php qui sert à compter le nombre d'éléments d'un tableau
echo '2.Les couleurs : <br>';
for ($i = 0; $i < count($couleurs); ++$i) {
    echo $couleurs[$i].'<br>';
}
echo '<br><br>';

//----------------------------------------
//Parcourir un tableau depuis la fin
echo '3.Les couleurs depuis la fin: <br>';
//n'oubliez pas qu'un tableau commence à l'index 0. C'est pour ça qu'on commence notre $i à la taille du tableau moins 1
for ($i = count($couleurs) - 1; $i >= 0; --$i) {
    echo $couleurs[$i].'<br>';
}
echo '<br><br>';
//----------------------------------------
//Parcourir un tableau multidimensionnel à 2 dimensions
$couleurs = array(
  array('rouge clair', 'rouge', 'rouge fonce'),
  array('bleu clair', 'bleu', 'bleu fonce'),
  array('vert clair', 'vert', 'vert fonce'),
  array('orange clair', 'orange', 'orange fonce'),
  array('marron clair', 'marron', 'marron fonce'),
);

echo '4.Les nuances de couleurs : <br>';
for ($i = 0; $i < count($couleurs); ++$i) {
    for ($j = 0; $j < count($couleurs[$i]); ++$j) {
        echo $couleurs[$i][$j].'<br>';
    }
}

echo '<br><br>';

/*****************************************/
/********** Les boucles : Foreach ********/
/*****************************************/
//parcourir un tableau simple
$couleurs = ['rouge', 'bleu', 'vert', 'orange', 'marron', 'noir', 'blanc'];
echo '5.Les couleurs : <br>';
foreach ($couleurs as $couleur) {
    echo $couleur.'<br>';
}
echo '<br><br>';

//----------------------------------------
//parcourir un tableau associatif
$vehicule = array(
  //clé => valeur
  'nom' => 'Aventador LP 700-4',
  'marque' => 'Lamborghini',
  'puissance' => 700,
  'prix' => 200000,
);
echo '6.Specificite de ma voiture : <br>';
//syntax : foreach($tableau as $cle => $valeur )
foreach ($vehicule as $propriete => $valeur) {
    echo $propriete.':'.$valeur.'<br>';
}
echo '<br><br>';

//----------------------------------------
//parcourir un tableau associatif multidimensionnel
$vehiculeConcession = array(
  'Bas de gamme' => array(
    'nom' => 'C1',
       'marque' => 'Citroen',
       'puissance' => 70,
       'prix' => 10000,
  ),
  'Milieu de gamme' => array(
    'nom' => 'Golf',
       'marque' => 'VW',
       'puissance' => 140,
       'prix' => 270000,
  ),
  'Haut de gamme' => array(
    'nom' => 'Aventador LP 700-4',
       'marque' => 'Lamborghini',
       'puissance' => 700,
       'prix' => 200000,
  ),
);
echo '7.Les voitures dans la concession :';
foreach ($vehiculeConcession as $gamme => $vehicule) {
    echo '<br>'.$gamme;
    foreach ($vehicule as $propriete => $valeur) {
        echo $propriete.' : '.$valeur.'<br>';
    }
}
echo '<br><br>';

/*###############################################*/
/*################# ACTIVITÉS ###################*/
/*###############################################*/

//----------------------------------------
//Créer un tableau pour les mois de l'année et affiché tous les mois de Janvier à Décembre
//modifier et/ou remplacer les éléments ci-dessous
echo "8.Les mois depuis le debut de l'annee : <br>";
$mois = ["janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"];
foreach ($mois as $mois) {
  echo $mois.'<br>';
}
echo '<br><br>';

//Afficher les mois de la fin de l'année jusqu'au début de l'année
//modifier et/ou remplacer les éléments ci-dessous
echo "9.Les mois depuis la fin de l'annee : <br>";
$mois = ["janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"];
for ($i = count($mois) - 1; $i >= 0; --$i) {
  echo $mois[$i].'<br>';
}
echo '<br><br>';
//----------------------------------------
//Afficher le nom et prénoms des élèves de ce collège
$college = array(
  'Sixieme' => array(
    array('Nom' => 'Payet', 'Prenom' => 'Mickael'),
    array('Nom' => 'Hoareau', 'Prenom' => 'Christine'),
    array('Nom' => 'Maillot', 'Prenom' => 'Laure'),
  ),
  'Cinquieme' => array(
    array('Nom' => 'Bourdon', 'Prenom' => 'Didier'),
    array('Nom' => 'Legitimus', 'Prenom' => 'Pascal'),
    array('Nom' => 'Campan', 'Prenom' => 'Bernard'),
    array('Nom' => 'Fois', 'Prenom' => 'Marina'),
    array('Nom' => 'Floresti', 'Prenom' => 'Florence'),
  ),
  'Quatrieme' => array(
    array('Nom' => 'Willis', 'Prenom' => 'Bruce'),
    array('Nom' => 'Lawrence', 'Prenom' => 'Laurence'),
    array('Nom' => 'Johannson', 'Prenom' => 'Scarlett'),
    array('Nom' => 'Jackson', 'Prenom' => 'Samuel'),
  ),
);

echo '<br><br>';

echo '10.Les eleves du college : <br>';
foreach( $college as $key => $value)
{
  echo $key. ': <br>';
//echo des intitulés 6eme 5eme 4eme

  foreach($value as $key2 => $liste)
{
  echo $key2. ': <br>';
//echo des numéro de tableau (payet mickael position 0...)

  foreach($liste as $nom => $valeur) {
    echo $nom . " : " . $valeur . ", <br>";
//echo de ce que contient le tableau 
  }
}
}

echo '<br><br>';

//----------------------------------------
//Afficher le nom et prénoms des élèves de ce collège
//reprenez le tableau ci-dessus, ajoutez des éléves pour la classe de troisième et réaffichez tout
echo '11.Les eleves du college (avec les nouveaux arrivants): <br>';
    $college['troisieme'] = array(
        array('Nom' => 'Patate', 'Prenom' => 'Jean'),
        array('Nom' => 'Crepe', 'Prenom' => 'Sophie'),
        array('Nom' => 'Chate', 'Prenom' => 'Romain'),

    );
    
foreach( $college as $key => $value)
{
  echo $key. ': <br>';

  foreach($value as $key2 => $liste)
{
  echo $key2. ': <br>';

  foreach($liste as $nom => $valeur) 
{
  echo $nom . " : " . $valeur . ", <br>";

  foreach($valeur as $nom => $total) 
{
  echo $nom . " : " . $valeur . ", <br>";
  }
  }
}
}

echo '<br><br>';

//----------------------------------------
//Afficher toutes les informations de la vidéothèque
$videotheque = array(
  array(
    'nom' => 'Independance day',
    'date' => 1996,
    'realisateur' => 'Roland Emmerich',
    'acteurs' => array(
      'Will Smith', 'Bill Pullman', 'Jeff Goldblum', 'Mary McDonnell',
    ),
  ),
  array(
    'nom' => 'Bienvenue a Gattaca',
    'date' => 1998,
    'realisateur' => 'Andrew Niccol',
    'acteurs' => array(
      'Ethan Hawke', 'Uma Thurman', 'Jude Law',
    ),
  ),
  array(
    'nom' => 'Forrest Gump',
    'date' => 1994,
    'realisateur' => 'Robert Zemeckis',
    'acteurs' => array(
      	'Tom Hanks', 'Gary Sinise',
    ),
  ),
  array(
    'nom' => '12 hommes en colere',
    'date' => 1957,
    'realisateur' => 'Sidney Lumet',
    'acteurs' => array(
      	'Henry Fonda','Martin Balsam','John Fiedler','Lee J. Cobb','E.G. Marshall',
    ),
  ),
);

echo '12.Mes films : <br>';

foreach ($videotheque as $description => $nom) {
  echo $description. ':<br>';

foreach($nom as $key => $liste)
{
  echo $key. ': <br>';
  if($key == 'acteurs'){
    foreach($liste as $acteurs) {
      echo $acteurs . ': <br>';
    }
  }

  else{
    echo $liste. ': <br>';
}
}
}
echo '<br><br>';

//----------------------------------------
//Afficher toutes les informations de la vidéothèque
//reprenez le tableau ci-dessus, ajoutez-y 3 de vos films préférés avec les mêmes
//d'informations (nom, date, realisateur, acteurs) et en plus de ces informations
//rajoutez un synopsis

echo '13.Mes films : <br>';
$videotheque[] = array(
    'nom' => 'La mule',
    'date' => 2018,
    'realisateur' => 'Clint Eastwood',
    'acteurs' => array(
      'Clint Eastwood', 'Bradley Cooper', 'Dianne Wiest',
    ),
    'synopsis' => "À plus de 80 ans, Earl Stone est aux abois. Il est non seulement fauché et seul, mais son entreprise risque d'être saisie. Il accepte alors d'être chauffeur, un petit boulot, en apparence, facile. Sauf que, sans le savoir, il s'est engagé à être passeur de drogue pour un cartel mexicain. Extrêmement performant, il transporte des cargaisons de plus en plus importantes. Ce qui pousse les chefs du cartel, toujours méfiants, à lui imposer un supérieur chargé de le surveiller. ",
  );
  $videotheque[] = array(
    'nom' => 'million dollar baby',
    'date' => 2005,
    'realisateur' => 'Clint Eastwood',
    'acteurs' => array(
        'Clint Eastwood', 'Hilary Swank', 'Morgan Freeman',
    ),
    'synopsis'=>'Rejeté depuis longtemps par sa fille, l entraîneur Frankie Dunn s est replié sur lui-même et vit dans un désert affectif. Le jour où Maggie Fitzgerald, 31 ans, pousse la porte de son gymnase à la recherche d un coach, elle n amène pas seulement avec elle sa jeunesse et sa force, mais aussi une histoire jalonnée d épreuves et une exigence, vitale et urgente : monter sur le ring, entraînée par Frankie, et enfin concrétiser le rêve d une vie.',
);
  
$videotheque[] = array(
    'nom' => 'Nos étoiles contraires',
    'date' => 2013,
    'realisateur' => 'John Green',
    'acteurs' => array(
        'Shailene Woodley','Nat Wolff','Laura Dern','Willem Dafoe','Ansel Elgort',
    ),
    'synopsis'=>'Depuis son enfance, Hazel a des problèmes respiratoires, l obligeant à porter un tube à oxygène en permanence. Sur les conseils de sa mère, elle participe à un groupe de soutien, où elle fait la connaissance d Augustus, qui a perdu une jambe à cause d un cancer. Charismatique et sûr de lui, le jeune homme devient rapidement proche d Hazel, lui montrant le bon côté des choses. Il arrive même à retrouver l écrivain préféré de sa nouvelle amie, planifiant un voyage à Amsterdam pour le rencontrer.',
  );

echo '<br><br>';

foreach ($videotheque as $description => $nom) {
  echo $description. ':<br>';

foreach($nom as $key => $liste)
{
  echo $key. ': <br>';
  if($key == 'acteurs'){
    foreach($liste as $acteurs) {
      echo $acteurs . ': <br>';
}
}
else{
  echo $liste. ': <br>';
}
}
}
echo '<br><br>';


